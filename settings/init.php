<?php
/**
 * all initial information, paths, globals	
 * created on 16.02.2007 by Roland Meyer
 */

// $OS = "macosx"
$OS = "linux";


$CWBDIR = "/opt/cwb-3.0/cqp/";
$PARCORPUSDIR = "/data/PolDeu/WALIGNED/";
$REGISTRY = "/data/PolDeu/WALIGNED/Registry";
$LANGPATH = "settings/languages.json";
$METAPATH = "settings/meta.json";

$CORPUSNAME = array ();
foreach (json_decode (file_get_contents ($LANGPATH)) as $lang)
{
	$CORPUSNAME[$lang->name] = $lang->corpus;
}

// only relevant for CWB HTML-based concordance
$CQPINIT = "settings/cqpinit";
$HARDBOUNDARY = "999";
$ENCODING = "UTF-8";

// only relevant for XML-based concordance
$ANNOTCONTEXT = 'show +tag; show +tag2; show +lemma; show +pl; show +de; set Context 1s;';
$metastructure = json_decode (file_get_contents ($METAPATH));

if (count($metastructure) > 0)
{
	$ANNOTCONTEXT .= ' set PrintStructures "';
	foreach ($metastructure as $field)
		$ANNOTCONTEXT .= $field->name.', ';
	$ANNOTCONTEXT = rtrim ($ANNOTCONTEXT, ", ");
	$ANNOTCONTEXT .= '";';
}

?>
