��          �      |      �     �  
          �        �  
          
        $     )     0     9  -   @  	   n     x          �     �     �     �     �  �  �     U     k  	   |  �   �     H     \     k  
   x     �     �     �  	   �  7   �     �     �     �                     2     5                  	                                                                        
       Advanced search CQP Search Clear Copyright &copy; 2015 Johannes Gutenberg-Universität  / Johannes Gutenberg University Mainz (Germany)<br>
			Research funded in 2014-2015 by Internal University Research Funding of the Johannes Gutenberg University Mainz Corpus Query interface Export XML German Gram. tag: Hide Lexeme Metadata Polish Polish-German / German-Polish Parallel Corpus Query for Search Set as primary language Show Token Tokens in between: from to Project-Id-Version: ParaVoz
POT-Creation-Date: 
PO-Revision-Date: 2015-03-25 08:17+0100
Last-Translator: 
Language-Team: 
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.5
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
 Szukanie zaawansowane Wyszukiwanie CQP Wyczyść Copyright &copy; 2015 Uniwersytet im. Jana Gutenberga w Moguncji (Niemcy)<br>
			Strona finansowana w latach 2014-2015 przez Fundusz Promocji Badań Uniwersytetu im. Jana Gutenberga w Moguncji  Zapytanie korpusowe Eksport do XML niemieckiego Tag gram.: Ukryj Leksem Metadane polskiego Polsko-Niemiecki / Niemiecko-Polski Korpus Równoległy Zapytanie dla języka Szukaj Ustaw jako język główny Pokaż Token Tokeny pomiędzy: od do 