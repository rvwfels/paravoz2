var corpus = angular.module('corpus', ['ngAnimate', 'ui.bootstrap', 'gettext', 'ngDialog']);


corpus.run(function (gettextCatalog) {
    gettextCatalog.setCurrentLanguage('en');
	gettextCatalog.debug = true;
});

corpus.controller('main', ['$scope','queryKeeper', 'gettextCatalog', 'ngDialog', function($scope, queryKeeper, gettextCatalog, ngDialog) {
	
	$scope.alignedLanguages = queryKeeper.getLanguages ();

    $scope.getAlignedLanguages = function() {
        
        result = [];
        
        result.push(queryKeeper.getPrimaryLanguage());
        
        for (lang in $scope.alignedLanguages) {
            if ($scope.alignedLanguages[lang] == true && result.indexOf(lang) < 0) result.push(lang);
        }
		
        return result;
    }
	    
    $scope.getPrimaryLanguage = function(){
        return queryKeeper.getPrimaryLanguage();
    }
	
	$scope.switchLanguage = function(lang) {
		gettextCatalog.setCurrentLanguage(lang);
	}

}]);

corpus.factory('queryKeeper', ['loadLanguages', 'loadMetaData', function(loadLanguages, loadMetaData) {

    var queryRows = [];
    var primaryLanguage;
	var metaFields = [];
	var cqpChanged = false;
	var alignedLanguages = {};
		
	loadMetaData.getJsonFile('settings/meta.json').then(function(response) {
		metaFields = angular.fromJson(response);
	});
	var queryNegation = [];

    return {
		
		clear: function(lang) {
			for (i = 0; i < queryRows[lang].length; ++i)
			{
				queryRows[lang][i].token = '';
				queryRows[lang][i].lexeme = '';
				queryRows[lang][i].gramTag = '';
			}
			for (i = 0; i < metaFields.length; ++i)
				metaFields[i].value = '';
		}, 
		getLanguages: function () {
			loadLanguages.getJsonFile('settings/languages.json').then(function(response) {
			var languages = angular.fromJson(response);
			for (i = 0; i < languages.length; ++i)
			{
				alignedLanguages[languages[i].name] = languages[i].use;
				if (languages[i].primary == true)
					primaryLanguage = languages[i].name;
			}
			});
			return alignedLanguages;
		},
		
        getPrimaryLanguage: function(){
            return primaryLanguage;
        },
        setPrimaryLanguage: function(newValue){
            primaryLanguage = newValue;
            return primaryLanguage;
        },
        get: function(lang, i) {
            return queryRows[lang][i];
        },
		getMeta: function(i, place) {
			return metaFields[i * 3 + place];
		},
		getMetaAll: function() {
			return metaFields;
		}, 
		getMetaLength: function() {
			return metaFields.length;
		},
		getNegation: function(lang) {
			return queryNegation[lang];
		},
        getAll: function(lang) {
            return queryRows[lang];
        },
		getCqpChanged: function () {
			return cqpChanged;
		},
        set: function(lang, index, item) {
            if (!(queryRows[lang] instanceof Array)) {
                queryRows[lang] = [];
            }

            queryRows[lang][index] = item;
            return item;
        },
		setMeta: function(i, place, item) {
			metaFields[i * 3 + place] = item;
			return item;
		},
		setNegation: function(lang, value) {
			queryNegation[lang] = value;
		},
		setCqpChanged: function (value) {
			cqpChanged = value;
		},
        pop: function(lang) {
            if (queryRows[lang].length > 1) queryRows[lang].pop();
        },
        push: function(lang) {
            if (!(queryRows[lang] instanceof Array)) {
                queryRows[lang] = [];
            }

            queryRows[lang].push({
                from: '',
                to: '',
                token: '',
                lexeme: '',
                gramTag: '',
                currentRowQueryStringWith: '',
                endWith: '',
                caseSensitive: false,
            });
        },
        length: function(lang) {
            return queryRows[lang].length;
        },
		metaLength: function() {
			return metaFields.length;
		},
        prepareQuery: function(lang) {
            var outputQuery = '';

            for (i in queryRows[lang]) {

                if (typeof(queryRows[lang][i]) == 'undefined') continue;
                var currentRowQueryString = '';
                queryRows[lang][i].token = queryRows[lang][i].token.replace(/ /g, "");
                queryRows[lang][i].lexeme = queryRows[lang][i].lexeme.replace(/ /g, "");
                queryRows[lang][i].gramTag = queryRows[lang][i].gramTag.replace(/ /g, "");


                if (queryRows[lang][i].token != '') {
                    if (queryRows[lang][i].endWith) currentRowQueryString += '.*';
                    currentRowQueryString += queryRows[lang][i].token;
                    if (queryRows[lang][i].beginWith) currentRowQueryString += '.*';

                    currentRowQueryString = 'word="' + currentRowQueryString + '"';

                    if (queryRows[lang][i].caseSensitive == false)
                        currentRowQueryString += "%c";
                }

                if (queryRows[lang][i].lexeme != '') {
                    if (currentRowQueryString != "") currentRowQueryString += " & ";
                    currentRowQueryString += ' lemma="' + queryRows[lang][i].lexeme + '"';
                }

                if (queryRows[lang][i].gramTag) {
                    if (currentRowQueryString != "") currentRowQueryString += " & ";
                    currentRowQueryString += ' tag="' + queryRows[lang][i].gramTag + '\"';
                }

                if (currentRowQueryString != '') {
					outputQuery += "[" + currentRowQueryString + "]";
                    if (queryRows[lang][i].from < queryRows[lang][i].to && queryRows[lang][i].to > 0) {
                        outputQuery += '[]{' + (queryRows[lang][i].from) + ',' + (queryRows[lang][i].to) + '}';
                    }					
                }
				if ((queryRows[lang][i].token != '' || queryRows[lang][i].lexeme != '' || queryRows[lang][i].gramTag != '') && queryNegation[lang]) outputQuery = '!' + outputQuery;
            }
			if (lang == primaryLanguage)
				return '@' + outputQuery;
			else
				return outputQuery;

        },
		addMeta: function() {
			
			var metaList = [];
			for (i = 0; i < metaFields.length; ++i)
			{
				if (metaFields[i].value == '') continue;
				metaList.push ('match.' + metaFields[i].name + '="' + metaFields[i].value + '"');
			}
			if (metaList.length != 0) return '::' + metaList.join(' & ');
			
			return '';
		}
    }

}]);

corpus.factory('stringProcessor', function() {
    return {
        removeSpaces: function(str) {
            if (typeof(str) == 'undefined') return '';
            var bufStr;
            var currentRowQueryString = 0;
            var end = str.length - 1;
            while (str[currentRowQueryString] == " ") currentRowQueryString++;
            while (str[end] == " ") end--;
            return str.substring(currentRowQueryString, end + 1);
        },
        replaceAndEscapeChrs: function(value) {
            if (typeof(value) == 'undefined') return '';
            value = value.replace(/\(/g, "\\(");
            value = value.replace(/\)/g, "\\)");
            value = value.replace(/\[/g, "\\[");
            value = value.replace(/\]/g, "\\]");
            value = value.replace(/\{/g, "\\}");
            value = value.replace(/\{/g, "\\}");
            value = value.replace(/\+/g, "\\+");
            value = value.replace(/\_/g, "\\_");
            value = value.replace(/\-/g, "\\-");
            value = value.replace(/\*/g, "\\*");
            value = value.replace(/\./g, "\\.");
            value = value.replace(/\:/g, "\\:");
            value = value.replace(/\;/g, "\\;");
            value = value.replace(/\,/g, "\\,");
            value = value.replace(/\?/g, "\\?");
            value = value.replace(/\!/g, "\\!");
            return value;
        }
    };
});

corpus.factory('autoCompleteDataService', ['$http', function() {
	
	var responseData = [];
	var lastQuery = '';
	
    return {
        getData: function(query, attribute, language) {
			
			query = query.replace(/ /g, "");
			if (query.length == 0)
				return responseData;
			query = '[' + attribute + '="' + query[0] + '.*"] ';
			if (query == lastQuery) 
				return responseData
			lastQuery = query;
			$http.get('autocomplete.php', {
				params: {query : query, attribute: at_type, language : language}
			})
			.success(function(data) {
				responseData = data;
			});
			
			return responseData;
		}
    };
}]);

corpus.factory('loadLanguages', ['$http', '$q', function(http, q) {
	
	return {
		getJsonFile: function(path) {
			var ldata;
			var deferred = q.defer();
			
			http.get(path).success (function(data) {
				deferred.resolve(data);
			});
			return deferred.promise;
		}
	};
}]);

corpus.factory('loadMetaData', ['$http', '$q', function(http, q) {
	
	return {
		getJsonFile: function(path) {
			var mdata;
			var deferred = q.defer();
			
			http.get(path).success (function(data) {
				deferred.resolve(data);
			});
			return deferred.promise;
		}
	};
}]);

corpus.factory('configData', ['gettext', function(gettext) {
	
	var langs = {bg: gettext("Bulgarian"),
	 by: gettext("Belorussian"),
	 cz: gettext("Czech"),
	 hr: gettext("Croatian"),
	 mk: gettext("Macedonian"),
	 pl: gettext("Polish"),
	 ru: gettext("Russian"),
	 sk: gettext("Slovak"),
	 sl: gettext("Slovene"),
	 sr: gettext("Serbian"),
	 uk: gettext("Ukrainian"),
	 us: gettext("Upper Sorbian"),
	 da: gettext("Danish"),
	 de: gettext("German"),
	 en: gettext("English"),
	 nl: gettext("Dutch"),
	 no: gettext("Norwegian"),
	 sv: gettext("Swedish"),
	 es: gettext("Spanish"),
	 fr: gettext("French"),
	 it: gettext("Italian"),
	 pt: gettext("Portuguese"),
	 ro: gettext("Romanian"),
	 lt: gettext("Lithuanian"),
	 lv: gettext("Latvian"),
	 ee: gettext("Estonian"),
	 el: gettext("Greek"),
	 eo: gettext("Esperanto"),
	 fi: gettext("Finnish"),
	 hu: gettext("Hungarian"),
	hy: gettext("Armenian")};
	 
	return {
		getLangs: function (lang) {
			return langs[lang];
		}
	};
}]);