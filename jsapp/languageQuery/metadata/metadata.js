corpus.directive('metadata', ['queryKeeper', 'gettextCatalog', function(queryKeeper, gettextcatalog) {
    return {
        templateUrl: 'jsapp/languageQuery/metadata/metadata.html',
        restrict: 'E',
        scope: {
			index: '=',
			place: '='
        },
        controller: function($scope, gettextCatalog) {
            $scope.meta = queryKeeper.getMeta($scope.index, $scope.place);
			//$scope.tip = gettextCatalog.getString('show') + ' <b> ' + $scope.meta.hint + '</b> ' + gettextCatalog.getString('in results');
            $scope.$watchCollection('meta', function(newValue, oldValue) {
                queryKeeper.setMeta($scope.index, $scope.place, newValue);
				if (newValue.inResults != oldValue.inResults)
				{
					console.log($scope.index);
					console.log($scope.place);
				}
            });

			$scope.getTip = function () {
				return gettextCatalog.getString('show') + ' <b> ' + $scope.meta.hint + '</b> ' + gettextCatalog.getString('in results');
			}
			
        }
    };
}]);