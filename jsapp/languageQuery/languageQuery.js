corpus.directive('languageQuery', ['stringProcessor', 'queryKeeper', 'configData', 'gettextCatalog', 'ngDialog', function(stringProcessor, queryKeeper, configData, gettextCatalog, ngDialog) {
    return {
        templateUrl: 'jsapp/languageQuery/languageQuery.html',
        restrict: 'E',
        scope: {
            language: '=',
        },
        controller: function($scope, gettextCatalog, configData) {
			
			var lastQuery = '';
			queryKeeper.setCqpChanged(false);
			
			$scope.metaVisible = false;
			$scope.metaButton = 'Show';
			
            $scope.fullNameOf = function(language){
                var translation = gettextCatalog.getString(configData.getLangs(language));
				if (gettextCatalog.getCurrentLanguage() == 'pl_PL' && translation.indexOf('i', translation.length - 1) !== -1)
					translation += 'ego';
                return translation;
            }
            
            $scope.exactPhraseQuery = function() {
                var quellText = stringProcessor.removeSpaces($scope.exactQuery);
                $scope.exactQuery = quellText;
                quellText = stringProcessor.replaceAndEscapeChrs(quellText);
                var resultText = "\"";
                var i = 0;
                // make from 'hallo you' query : '"hallo" "you"'
                while (i < quellText.length) {
                    var ch = quellText.charAt(i);
                    if (ch == ' ') {
                        resultText += "\"";
                    }
                    resultText += ch;
                    if (ch == ' ') {
                        resultText += "\"";
                    }
                    i++;
                }
                resultText += "\"";
                if ($scope.exactQueryCaseSensitive == true)
                    resultText = resultText + "%c";
                $scope.exactQueryOutput = resultText;
            }

            $scope.$watchCollection('{eq: exactQuery, cs: exactQueryCaseSensitive}', $scope.exactPhraseQuery);
			
			$scope.getVisible = function() {
				return $scope.metaVisible;
			}
			
			$scope.changeVisible = function() {
				$scope.metaVisible = !$scope.metaVisible;
				if ($scope.metaVisible == true)
					$scope.metaButton = 'Hide';
				else
					$scope.metaButton = 'Show';
			}
			
			$scope.clear = function() {
				queryKeeper.clear($scope.language);
			}

            $scope.moreRows = function() {
                queryKeeper.push($scope.language);
            };

            $scope.lessRows = function() {
                queryKeeper.pop($scope.language);
            };

            $scope.rowsNumber = function() {
            	var myArr = [];
            	for (i=0; i<queryKeeper.length($scope.language); myArr.push(i++));
            	return myArr;
            }

            $scope.allRows = function(){
            	return queryKeeper.getAll($scope.language);
            }
			
            $scope.getQuery = function(){
				if (queryKeeper.getCqpChanged ()) // if Cqp Search Field was manually edited, don't update it without user confirmation
				{
					return $scope.outputQuery;
				}
            	var query = queryKeeper.prepareQuery($scope.language);
				
				if (query != '' && $scope.language == $scope.getPrimary()) query += queryKeeper.addMeta();
				
				return query;
            }
			
			$scope.checkManualUpdate = function() {
				queryKeeper.setCqpChanged(true);
			}
            
            $scope.getPrimary = function(){
                return queryKeeper.getPrimaryLanguage();
            }
            
            $scope.setPrimary = function(newValue){
                return queryKeeper.setPrimaryLanguage(newValue);
            }
			
			$scope.getButtonState = function() {
				return gettextCatalog.getString($scope.metaButton);
			}
			
			$scope.getMetaVisibility = function(){
				var resp = "";
				if (queryKeeper.getPrimaryLanguage() != $scope.language)
					resp = "hidden";
				return resp;
				
			}
			
			$scope.metaToShow = function()
			{
				var meta = queryKeeper.getMetaAll();
				var results = [];
				for (var i = 0; i < meta.length; ++i)
				{
					if (meta[i].inResults == true)
						results.push(meta[i].name);
				}
				return JSON.stringify(results);
			}
			
			$scope.queryNegation = queryKeeper.getNegation ($scope.language);
			
			$scope.$watch('queryNegation', function(newValue) {
				queryKeeper.setNegation($scope.language, newValue);
			});

            $scope.$watch('getQuery()',function(nval){
                $scope.outputQuery = nval;
            });
			
			$scope.updateQuery = function() {
			}
			
            //initialize query rows
            $scope.moreRows();
            //$scope.moreRows();

            $scope.exactQueryCaseSensitive = true;
			
			$scope.metaFieldsNumber = function() {
				return queryKeeper.metaLength();
			}
			
			$scope.metaFieldsNumberArray = function() {
				var tmpArray = [];
				for (i = 0; i < queryKeeper.metaLength() / 3; tmpArray.push(i++));
				return tmpArray;
			}

        }
    };
}]);