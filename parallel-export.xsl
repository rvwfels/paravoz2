<?xml version="1.0" encoding="UTF-8"?>
<!-- THIS IS THE XSLT SHEET TO TRANSFORM QUERY RESULTS FOR EXPORT-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2003/11/xpath-functions">
	<xsl:output method="html"/>
		<xsl:template match="/">
			<html>
				<head>
				</head>
				<body>
					<p align="left" style="font-family:Tahoma; font-size:12px; color:black">
					<b/>
					<xsl:apply-templates/>

					</p>
				</body>
			</html>
		</xsl:template>

		
<xsl:template match="CONCORDANCE">
					<table id="ausgabetabelle" style="font-size: small; border-spacing: 1; background-color: #FFEEAA; width: 100%;"> 
					<tr><td>
					<xsl:element name="b">					
					<!-- <xsl:variable name="num-LINE" select="count(.//LINE)"/> 
					<xsl:value-of select="$num-LINE"/>
					<xsl:value-of select="' hit'"/>
					<xsl:if test="$num-LINE>1">
					<xsl:value-of select="'s'"/>
					</xsl:if>
					-->
					<xsl:value-of select="'Corpus '"/>
					<xsl:element name="i">					
					<xsl:value-of select="substring-before(/RESULTS/@corpus, '_')"/>
					<xsl:value-of select="', '"/>
					</xsl:element>									
					<xsl:value-of select="'language '"/>
					<xsl:element name="i">					
					<xsl:value-of select="substring-after(/RESULTS/@corpus, '_')"/>
					<xsl:value-of select="'.'"/>
					</xsl:element>									
					</xsl:element>									
					</td></tr>
					<!-- Überschrift -->
					<xsl:element name="tr">
					<xsl:element name="td">
					<xsl:value-of select="/RESULTS/@primlang"/>
					</xsl:element>
					<xsl:variable name="fpos" select="count (LINE[1]/following-sibling::ALIGN) - count (LINE[2]/following-sibling::ALIGN)"/>
					<xsl:for-each select="LINE[1]/following-sibling::ALIGN[position() &lt;= $fpos]">
					<xsl:element name="td">
					<xsl:value-of select="substring-after(@name, '_')"/>
					</xsl:element>
					</xsl:for-each>
					</xsl:element>
					
					<xsl:variable name="npos" select="count (LINE[1]/following-sibling::ALIGN) - count (LINE[1]/following-sibling::LINE/following-sibling::ALIGN)"/>
					<xsl:variable name="numberColumns" select="count (LINE[1]/following-sibling::ALIGN[position() &lt;= $npos])"/>


					
					<xsl:for-each select="LINE">
					<xsl:element name="tr">
					<xsl:if test="(position() mod 2)=1">
					<xsl:attribute name="style">color:blue; background-color: #FFFFFF;</xsl:attribute>
					</xsl:if>
					<xsl:if test="(position() mod 2)=0">
					<xsl:attribute name="style">color:blue; background-color: #FFDDAA;</xsl:attribute>
					</xsl:if>
					
					
					<xsl:element name="td">
					<xsl:apply-templates/>
					</xsl:element>
					<xsl:for-each select="following-sibling::ALIGN[position() &lt;= $npos]">
					<xsl:element name="td">
					<xsl:attribute name="width">
					<xsl:value-of select="(100 div ($numberColumns+1))"/>
					<xsl:value-of select="'%'"/>
					</xsl:attribute>
					<!-- 					<xsl:value-of select="position()"/>  -->
					<xsl:attribute name="title"></xsl:attribute>
					<xsl:apply-templates/>
					</xsl:element>
					</xsl:for-each>
					</xsl:element>
					</xsl:for-each>
					</table>					
</xsl:template>



<xsl:template match="MATCHNUM">
<font color="red">
<xsl:apply-templates/>
</font>
</xsl:template>

<xsl:template match="MATCH">
<font color="red" >
<b>
<xsl:apply-templates/>
</b>
</font>
</xsl:template>



<xsl:template match="TOKEN">
<xsl:element name="font">
<xsl:attribute name="title">
<xsl:value-of select="ANNOT"/>
</xsl:attribute>
<xsl:value-of select="text()"/>
</xsl:element>
</xsl:template>

<xsl:template match="s">
<xsl:element name="sup">
<xsl:value-of select="@id"/>
</xsl:element>
</xsl:template>
	</xsl:stylesheet>
