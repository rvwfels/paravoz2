<?php 
/* created by Andreas Zeman and Ruprecht von Waldenfels
 * this is the usual result page, outputting XML which is then transformed using XSLT
 */ 
// retrieve defaults

$CQPOPTIONS = " ";
if ($CQPINIT) {
    $CQPOPTIONS .= " -I $CQPINIT";
};
if ($HARDBOUNDARY) {
    $CQPOPTIONS .= " -b $HARDBOUNDARY";
}

// Sucheingabe umsetzen in cqp

$actquery = 'set Context ' . $kontextnum . ' ' . $kontexttyp . '; ' . $CORPUSNAME[$primlang] . '; '.$ANNOTCONTEXT;

foreach ($langs as $l) {
	if ($l != $primlang)
		$actquery .= 'show +' . strtolower ($CORPUSNAME[$l]) . '; ';
}
$actquery .= $query[$primlang];

foreach ($langs as $l) {
	if (($l != $primlang) && $query[$l])
		$actquery .= ': ' . $CORPUSNAME[$l] . ' ' . $query[$l];
}
$actquery .= ' ; ;size Last;';

$out = array();
$execstring = "$CWBDIR" . "cqpcl -r $REGISTRY" . " 'set PrintMode sgml; " . $actquery;
$execstring .= "'";

if ($OS == "linux") {
	
	foreach (json_decode($_POST['metaToShow']) as $meta)
		$execstring .= " | sed -r 's/&lt;(".$meta.") *([^&]*)&gt;/<show \\1=\"\">\\2<\/show>/g'";
		
    $execstring .= " | sed -r 's/(&|&amp;)lt;(\/)?s(_id [0-9]+)?(&|&amp;)gt;//g'";
    $execstring .= " | sed -r 's/^<align (.*)$/<ALIGN \\1 <\/ALIGN>/g'";
    $execstring .= " | sed -r 's/\/__UNDEF__//g'";
    $execstring .= " | sed -r 's/&lt;TOKEN&gt;/<TOKEN>/g'";
    // $execstring .= " | sed -r 's/&lt;(chapter_author) *([^&]*)&gt;/<author>\\2<\/author>/g'";
	// $execstring .= " | sed -r 's/&lt;(chapter_title) *([^&]*)&gt;/<title>\\2<\/title>/g'";
	// $execstring .= " | sed -r 's/&lt;(chapter_origtitle) ([^&]*)&gt;/<origingal_title>\\2<\/original_title>/g'";
	// $execstring .= " | sed -r 's/&lt;(chapter_translator) ([^&]*)&gt;/<translator>\\2<\/translator>/g'";
	// $execstring .= " | sed -r 's/&lt;(chapter_work) ([^&]*)&gt;/<work>\\2<\/work>/g'";
	// $execstring .= " | sed -r 's/&lt;(chapter_pub_year) ([^&]*)&gt;/<publication_year>\\2<\/publication_year>/g'";
	// $execstring .= " | sed -r 's/&lt;(chapter_trans_year) ([^&]*)&gt;/<translation_year>\\2<\/translation_year>/g'";
	// $execstring .= " | sed -r 's/&lt;(chapter_src) ([^&]*)&gt;/<source_language>\\2<\/source_language>/g'";
    $execstring .= " | sed -r 's/&lt;\/TOKEN&gt;/<\/TOKEN>/g'";
    $execstring .= " | sed -r 's/(<TOKEN>.[^\/<]*)\/([^>]+)<\/TOKEN>/\\1<ANNOT>\\2<\/ANNOT><\/TOKEN>/g'";
    $execstring .= " | sed -r 's/&lt;CONTENT&gt;(.*)&lt;\/CONTENT&gt;/<CONTENT>\\1<\/CONTENT>/g'";
    $execstring .= " | sed -r 's/<attribute[^>]+>//g'";
    $execstring .= " | sed -r 's/&(amp;)+(quot;|amp;)/\&\\2/g'";
   
} elseif ($OS = "macosx") {   


    $execstring .= " | sed -E 's/(&|&amp;)lt;(\/)?s(_id [0-9]+)?(&|&amp;)gt;//g'";
    $execstring .= " | sed -E 's/^<align (.*)$/<ALIGN \\1 <\/ALIGN>/g'";
    $execstring .= " | sed -E 's/\/__UNDEF__//g'";
    $execstring .= " | sed -E 's/(<TOKEN>[^\/<]+)\/([^>]+)<\/TOKEN>/\\1<ANNOT>\\2<\/ANNOT><\/TOKEN>/g'";
    $execstring .= " | sed -E 's/<attribute[^>]+>//g'";
    $execstring .= " | sed -E 's/&(amp;)+(quot;|amp;)/\&\\2/g'";
    $execstring .= "\n";
}  

unset($outtmp);
exec($execstring, $outtmp);
if (preg_match("/\s*\d+\s*$/", end($outtmp))){
		$out=array_merge($out, $outtmp);
}

        
            
    //to do: check for mistakes (check number of results!)

$outstr = trim(implode("\n", $out));
$outstr = '<RESULTS primlang="'.$primlang.'">'.$outstr ;
$outstr .= '</RESULTS>';

if (isset($_POST['btn']['xml'])){
    header('Content-type: text/xml; charset=utf-8'); 
        
    echo('<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="./parallel-kwic.xsl" ?>');
                       
    echo ($outstr);
} else {
    header('Content-type: text/xml; charset=utf-8');
    header('Content-Disposition: attachment; filename="results.xml"' );
    echo('<?xml version="1.0"?>' . $outstr);
};
?>
