<?xml version="1.0" encoding="UTF-8"?>
<!-- THIS IS THE XSLT SHEET TO TRANSFORM QUERY RESULTS FOR DISPLAY IN A CONCORDANCE WINDOW-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2003/11/xpath-functions">
	<xsl:output method="html"/>
	<xsl:strip-space elements="*"/>
	<xsl:template match="/">
			<html>
				<head>
				<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
				<!--
				 <script type="text/javascript">
				 $(document).ready(function() {
					$('td').click(function() {
						var myCol = $(this).index();
						var $tr = $(this).closest('tr');
						var myRow = $tr.index();
						var table = $(this).closest('table');
						var lang = $(table).find("tr").eq(1).find("td").eq(myCol).html();
						var primlang = $(table).find("tr").eq(1).find("td").eq(0).html();
						var token_nr = $(table).find("tr").eq(myRow).find("td").eq(0).find("font").eq(0).html();
						var corpus = $(table).find("i").eq(0).html();
						var url = 'results_context_xml.php?corpus=' + corpus + '&amp;lang=' + lang + '&amp;primlang=' + primlang + '&amp;token_nr=' + token_nr;
						var windowName = corpus + "_" + primlang + ":" + token_nr + " in " + corpus + "_" + lang;
						var windowSize = "width=300,height=400,scrollbars=yes";
						window.open(url, windowName, windowSize);
					});
				  });
				 </script>
				 -->
				</head>
				<body>
				<!--<xsl:element name="font">
Metadata: <xsl:attribute name="size">+1</xsl:attribute>
<xsl:attribute name="color">black</xsl:attribute>
<xsl:for-each select="RESULTS/CONCORDANCE/LINE[1]/STRUCS/show">
<xsl:value-of select="name(@*)"/>
<xsl:if test="position() &lt; last()">
<xsl:value-of select="', '"/>
</xsl:if>
</xsl:for-each>
</xsl:element>-->


					<p align="left" style="font-family:Tahoma; font-size:12px; color:black">
					<b/>
					<xsl:apply-templates/>
					<xsl:variable name="ALLnum-LINE" select="count(//LINE)"/>
					<xsl:value-of select="$ALLnum-LINE"/>
					<xsl:value-of select="' hits overall.'"/> 

					</p>
				</body>
			</html>
		</xsl:template>

		<xsl:template match="RESULTS/text()">
		<xsl:element name="b">
					<xsl:value-of select="'('"/>
			<xsl:value-of select="."/>
					<xsl:value-of select="' hit'"/>
					<xsl:if test=".&gt;1">
					<xsl:value-of select="'s'"/>
					</xsl:if>
					<xsl:value-of select="'.)'"/>
					
		</xsl:element>
		<xsl:element name="p"/>
		</xsl:template>
		
<xsl:template match="CONCORDANCE">
					<table id="ausgabetabelle" style="font-size: small; border-spacing: 1; background-color: #FFEEAA; width: 100%;"> 
					<tr><td>
					<xsl:element name="b">					
					<!-- <xsl:variable name="num-LINE" select="count(.//LINE)"/> 
					<xsl:value-of select="$num-LINE"/>
					<xsl:value-of select="' hit'"/>
					<xsl:if test="$num-LINE>1">
					<xsl:value-of select="'s'"/>
					</xsl:if>
					-->
					<!--
					<xsl:value-of select="'Metadata format: '"/>
					<xsl:call-template name="meta-format"/>
					</xls:call-template>
					-->
					</xsl:element>									
					</td></tr>
					<!-- Überschrift -->
					<xsl:element name="tr">
					<xsl:element name="td">
					<b>
					<xsl:value-of select="/RESULTS/@primlang"/>
					</b>
					</xsl:element>
					<xsl:variable name="fpos" select="count (LINE[1]/following-sibling::ALIGN) - count (LINE[2]/following-sibling::ALIGN)"/>
					<xsl:for-each select="LINE[1]/following-sibling::ALIGN[position() &lt;= $fpos]">
					<xsl:element name="td">
					<b>
					<xsl:value-of select="substring-after(@name, '_')"/>
					</b>
					</xsl:element>
					</xsl:for-each>
					</xsl:element>
					
					<xsl:variable name="npos" select="count (LINE[1]/following-sibling::ALIGN) - count (LINE[1]/following-sibling::LINE/following-sibling::ALIGN)"/>
					<xsl:variable name="numberColumns" select="count (LINE[1]/following-sibling::ALIGN[position() &lt;= $npos])"/>


					
					<xsl:for-each select="LINE">
					<xsl:element name="tr">
					<xsl:if test="(position() mod 2)=1">
					<xsl:attribute name="style">color:blue; background-color: #FFFFFF;</xsl:attribute>
					</xsl:if>
					<xsl:if test="(position() mod 2)=0">
					<xsl:attribute name="style">color:blue; background-color: #FFDDAA;</xsl:attribute>
					</xsl:if>
					
					<xsl:variable name="aligInfo" select="CONTENT/MATCH/COLLOCATE"/>

					<xsl:element name="td">
					<xsl:apply-templates select="CONTENT"/>
					<xsl:apply-templates select="STRUCS"/>
					</xsl:element>
					<xsl:for-each select="following-sibling::ALIGN[position() &lt;= $npos]">
					<xsl:element name="td">
					<xsl:attribute name="width">
					<xsl:value-of select="(100 div ($numberColumns+1))"/>
					<xsl:value-of select="'%'"/>
					</xsl:attribute>
					<!-- 					<xsl:value-of select="position()"/>  -->
					<xsl:attribute name="title"></xsl:attribute>
					<xsl:call-template name="doToken">
					<xsl:with-param name="aligInfo" select="$aligInfo"/>
					</xsl:call-template>
					</xsl:element>
					</xsl:for-each>
					</xsl:element>
					</xsl:for-each>
					</table>					
</xsl:template>

<xsl:template name="doToken">
<xsl:param name="aligInfo"/>
<xsl:for-each select=".//TOKEN">
<xsl:if test="not ((starts-with(text(),  '.')) or (starts-with( text(), '?')) or (starts-with(text(), '!')) or (starts-with(text(), ',')) or (starts-with(text(), ':')) or (starts-with(text(), ';')) or (starts-with(text(), ':')))">
<xsl:value-of select="' '"/>
</xsl:if>
<xsl:variable name="id" select="substring-before(substring-after(substring-after(ANNOT,'/'),'/'), '/')"/>
<xsl:variable name="PASS" select="concat('=w:', text(), '=i:', $id)"/>


<!-- <xsl:message><xsl:value-of select="$PASS"/></xsl:message>-->
<!-- <xsl:message><xsl:value-of select="$PASS"/></xsl:message> -->
<xsl:element name="font">
<xsl:attribute name="title">
<xsl:value-of select="ANNOT"/>
</xsl:attribute>

<xsl:if test="contains($aligInfo/TOKEN/ANNOT, $PASS)">
<xsl:attribute name="style">font-weight: bold</xsl:attribute>
</xsl:if>
<xsl:value-of select="text()"/>
</xsl:element>
</xsl:for-each>
</xsl:template>

<!--
<xsl:template name="metaFormat">
<xsl:param name="strucsNode"/>
<xsl:for-each select="show">
<xsl:value-of
</xsl:for-each>
</xsl:template>
-->


<xsl:template match="MATCHNUM">
<font color="red">
<xsl:apply-templates/>
</font>
</xsl:template>

<xsl:template match="MATCH">
<font color="red" >
<b>
<xsl:apply-templates/>
</b>
</font>
</xsl:template>

<xsl:template match="STRUCS">
<!--
<xsl:element name="font">
<xsl:attribute name="size">-4</xsl:attribute>
<xsl:attribute name="color">black</xsl:attribute>
<xsl:attribute name="title">
<xsl:for-each select="show[not(@chapter_author or @chapter_title or @chapter_pub_year)]">
<xsl:value-of select="."/>
<xsl:if test="position() &lt; last()">
<xsl:value-of select="' : '"/>
</xsl:if>
</xsl:for-each>
</xsl:attribute>
[
<xsl:if test="show[@chapter_author]">
<xsl:value-of select="show[@chapter_author]"/>
<xsl:value-of select="': '"/>
</xsl:if>
<xsl:if test="show[@chapter_title]">
<xsl:value-of select="show[@chapter_title]"/>
</xsl:if>
<xsl:if test="show[@chapter_pub_year]">
<xsl:value-of select="show[@chapter_pub_year]"/>
</xsl:if>
]
</xsl:element>
-->
<xsl:element name="font">
<xsl:attribute name="size">-4</xsl:attribute>
<xsl:attribute name="color">black</xsl:attribute>
<xsl:attribute name="title">
<xsl:for-each select="show">
<xsl:value-of select="name(@*[1])"/>
<xsl:value-of select="': '"/>
<xsl:value-of select="."/>
<xsl:if test="position() &lt; last()">
<xsl:value-of select="'&#10;'"/>
</xsl:if>
</xsl:for-each>
</xsl:attribute>

<xsl:value-of select="' ['"/>
<xsl:for-each select="show">
<xsl:value-of select="."/>	
<xsl:if test="position() &lt; last() and string-length(.) != 0">
<xsl:value-of select="', '"/>
</xsl:if>
</xsl:for-each>
<xsl:value-of select="']'"/>
</xsl:element>



</xsl:template>


<xsl:template match="TOKEN">
<xsl:if test="not ((starts-with(text(),  '.')) or (starts-with( text(), '?')) or (starts-with(text(), '!')) or (starts-with(text(), ',')) or (starts-with(text(), ':')) or (starts-with(text(), ';')) or (starts-with(text(), ':')))">
<xsl:value-of select="' '"/>
</xsl:if>
<xsl:element name="font">
<xsl:attribute name="title">
<xsl:value-of select="ANNOT"/>
</xsl:attribute>
<xsl:value-of select="text()"/>
</xsl:element>
</xsl:template>

<xsl:template match="s">
<xsl:element name="sup">
<xsl:value-of select="@id"/>
</xsl:element>
</xsl:template>
	</xsl:stylesheet>
