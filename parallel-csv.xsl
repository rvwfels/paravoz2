<?xml version="1.0" encoding="UTF-8"?>
<!-- THIS IS THE XSLT SHEET TO TRANSFORM QUERY RESULTS FOR EXPORT CVS -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2003/11/xpath-functions">
	<xsl:output method="xml"/>
		<xsl:template match="/">
					<xsl:for-each select="//CONCORDANCE">
					<xsl:element name="text">	
					<!-- Überschrift -->
					<xsl:variable name="fpos" select="count (LINE[1]/following-sibling::ALIGN) - count (LINE[2]/following-sibling::ALIGN)"/>
					<xsl:for-each select="LINE[1]/following-sibling::ALIGN[position() &lt;= $fpos]">
					<xsl:value-of select="substring-after(@name, '_')"/>
					<xsl:string>	</xsl:string>	
					</xsl:for-each>
					<xsl:value-of select="'&#13;&#10;'"/>
					<xsl:value-of select="'&#13;&#10;'"/>
				<xsl:variable name="npos" select="count (LINE[1]/following-sibling::ALIGN) - count (LINE[1]/following-sibling::LINE/following-sibling::ALIGN)"/>
					<xsl:variable name="numberColumns" select="count (LINE[1]/following-sibling::ALIGN[position() &lt;= $npos])"/>


					<xsl:for-each select="LINE">
					<xsl:apply-templates/>
					<xsl:for-each select="following-sibling::ALIGN[position() &lt;= $npos]">
					<xsl:value-of select="'&09;'"/>
					<xsl:apply-templates/>
		
					</xsl:for-each>
					<xsl:value-of select="'&#13;&#10;'"/>
					</xsl:for-each>
					<xsl:value-of select="'&#13;&#10;'"/>
					</xsl:for-each>
		</xsl:template>

<xsl:template match="MATCHNUM">
</xsl:template>

<xsl:template match="MATCH">-&gt;<xsl:apply-templates/>&lt;-</xsl:template>



<xsl:template match="TOKEN">
<xsl:value-of select="ANNOT[1]"/>
</xsl:template>

<xsl:template match="s">
</xsl:template>
	</xsl:stylesheet>
