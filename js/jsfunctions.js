// includes functions for the language choice components on the query page
var sourceLang // enthält die aktuelle source language

function setSourceLang (f) {
	for (var j=0; j<f.primlang.length; j++){
		if (f.primlang[j].checked){
			sourceLang = f.primlang[j].name
		}
	}
}

function MakeArray(n){
   for (var i=0; i<n; i++)
      this[i] = 0
   this.length = n
}

function syncPrimlang(f,t){
	var selLangs = document.getElementsByName('langs[]');
	var selLangslaenge = selLangs.length;
	for(var i=0;i<selLangslaenge;i++) { 
		if(selLangs[i].value == t){
			if(selLangs[i].checked == false){ f.primlang[i].checked = false }
		}
	};
    chkForm(f)
}

function markLang(f){
	var selLangs = document.getElementsByName('langs[]');
	var selLangslaenge = selLangs.length;
	for(var i=0;i<selLangslaenge;i++) { 
			if(f.primlang[i].checked == true){ selLangs[i].checked = true }
	};
    chkForm(f)
}

function chkForm (f) {
	var ok1 = false
	var ok2 = false
	var selLangs = document.getElementsByName('langs[]');
	var selLangslaenge = selLangs.length;
	for (var j=0; j<f.primlang.length; j++){
		if (f.primlang[j].checked){	ok1 = true }
	}
	for(var i=0;i<selLangslaenge;i++) { 
		if((selLangs[i].checked)&&(!f.primlang[i].checked)){ ok2 = true }
	}	
//	if(ok1 && ok2){ document.forms["languages"].submit() } else {
	if(ok1 && ok2){ return true } else {
		alert('Please select a source language and at least one target language!')
		f.focus()
	}
}

function chkAll(t) {
	var seltexts = document.getElementsByName(t);
	var seltextslaenge = seltexts.length;
	for(var i=0;i<seltextslaenge;i++) { 
		seltexts[i].checked = true
	}
}

function chkNone(t) {
	var seltexts = document.getElementsByName(t);
	var seltextslaenge = seltexts.length;
	for(var i=0;i<seltextslaenge;i++) { 
		seltexts[i].checked = false
	}
}

function Go (select) {
	var wert = select.options[select.options.selectedIndex].value;
	if (wert == "leer") {
    	select.form.reset();
	    parent.frames["kwic"].focus();
    	return;
	} else {
		parent.frames["kwic"].location.href = wert;
		select.form.reset();
		parent.frames["kwic"].focus();
    }
}

// Autocomplete query will be saved here
var globalQuery="xx";


// Extract query from input-field (or other given string)
// Example: string str "make it" functions return query-string '"m.*"[]{0,0}"i.*"'
function extractQuery(str, at_type)
{
    var query = "";
    var i = 0;
    //alert ('Str: ' + str + ' end');
    // returns empty string if str is empty or begins with space or tab 
    if (str.length < 1 || str[i]==" " || str[i]=="\t") return ""; 
    // extract query character or the first word 
    query = '[' +at_type+ '="' + str[i] + '.*"] ';
    i++;
    while(i < str.length)
    {
        if (str[i]==" ")
        {
            i++;
            if (i >= str.length || str[i]==" " || str[i]=="\t") return query;
     					query = ' [' +at_type+ '="' + str[i] + '.*"] ';
        }
        i++;
    }
    return query;	 
}

// Ajax-connector between autocomplete.php and UserInterface
function getSuggestion(id, at_type, language)
{
    var query = extractQuery(document.getElementById(id).value, at_type);
//	console.log(query);
    if (query.length > 0 && query !== globalQuery)
    {
        globalQuery = query;
        $.ajax({	
            url : 'autocomplete.php',
            dataType : 'json',
            data: {
                query : globalQuery,
                attribute : at_type,
				language : language
            }, 
            success: loadData
        });
    }
}

// List of suggestions/words/phrases, wich was sent from server
var dataAutocomplete;

// Load words and phrases, wich we have got from server (autocomplete.php) to jquery-autocomplete
function loadData(data, textStatus, jqXHR)
{ 
    dataAutocomplete = data;
	//alert(data);
    $( ".keyboardInput" ).autocomplete({
        source: dataAutocomplete,
		select: function(event, ui) {
			if (angular.element($(this)).scope().atype == 'word')
				angular.element($(this)).scope().queryRow.token = ui.item.value;
			else
				angular.element($(this)).scope().queryRow.lexeme = ui.item.value;
			angular.element($(this)).scope().$apply();
		}
    });
    $( ".keyboardInput" ).autocomplete( "option", "minLength", 2);
    $( ".keyboardInput" ).autocomplete( "option", "delay", 0);

}

function confirmDialog() {
    // Define the Dialog and its properties.
	console.log('cd');
    $("#dialog-confirm").dialog({
        resizable: false,
        modal: true,
        title: "Modal",
        height: 250,
        width: 400,
        create: function (e, ui) {
            var pane = $(this).dialog("widget").find(".ui-dialog-buttonpane")
            $("<label class='shut-up' ><input  type='checkbox'/> Stop asking!</label>").prependTo(pane)
        },
        buttons: {
            'a': function () {
                $(this).dialog('close');
                return true;
            },
                'b': function () {
                $(this).dialog('close');
                return false;
            }
        }
    });
}

